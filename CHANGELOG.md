1.0.1 - 2020-04-06
---
* Avoid name space clash on global variables with puppet-lint-legacy

1.0.0 - 2020-04-06
---
* initial release very much inspired by https://github.com/mmckinst/puppet-lint-legacy_
