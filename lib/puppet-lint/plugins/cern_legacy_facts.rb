PuppetLint.new_check(:cern_legacy_facts) do
  CERN_LEGACY_FACTS_VAR_TYPES = Set[:VARIABLE, :UNENC_VARIABLE]

  # These facts have a one to one correlation between a legacy fact and a new
  # structured fact.
  CERN_EASY_FACTS = {
    'operatingsystemmajorrelease' => "facts['os']['release']['major']",
  }

  # A list of valid hash key token types
  CERN_HASH_KEY_TYPES = Set[
    :STRING,  # Double quoted string
    :SSTRING, # Single quoted string
    :NAME,    # Unquoted single word
  ].freeze

  def check
    tokens.select { |x| CERN_LEGACY_FACTS_VAR_TYPES.include?(x.type) }.each do |token|
      fact_name = ''

      # Get rid of the top scope before we do our work. We don't need to
      # preserve it because it won't work with the new structured facts.
      if token.value.start_with?('::') then
        fact_name = token.value.sub(/^::/, '')

      # This matches using legacy facts in a the new structured fact. For
      # example this would match 'uuid' in $facts['uuid'] so it can be converted
      # to facts['dmi']['product']['uuid']"
      elsif token.value == 'facts' then
        fact_name = hash_key_for(token)

      elsif token.value.start_with?("facts['")
        fact_name = token.value.match(/facts\['(.*)'\]/)[1]
      end

      if CERN_EASY_FACTS.include?(fact_name) then
        notify :warning, {
          :message   => 'cern legacy fact',
          :line      => token.line,
          :column    => token.column,
          :token     => token,
          :fact_name => fact_name,
        }
      end
    end
  end

  # If the variable is using the $facts hash represented internally by multiple
  # tokens, this helper simplifies accessing the hash key.
  def hash_key_for(token)
    lbrack_token = token.next_code_token
    return '' unless lbrack_token && lbrack_token.type == :LBRACK

    key_token = lbrack_token.next_code_token
    return '' unless key_token && CERN_HASH_KEY_TYPES.include?(key_token.type)

    key_token.value
  end

  def fix(problem)
    fact_name = problem[:fact_name]

    # Check if the variable is using the $facts hash represented internally by
    # multiple tokens and remove the tokens for the old legacy key if so.
    if problem[:token].value == 'facts'
      loop do
        t = problem[:token].next_token
        remove_token(t)
        break if t.type == :RBRACK
      end
    end

    if CERN_EASY_FACTS.include?(fact_name)
      problem[:token].value = CERN_EASY_FACTS[fact_name]
    end
  end
end
