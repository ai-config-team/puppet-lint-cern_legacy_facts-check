Gem::Specification.new do |spec|
  spec.name        = 'puppet-lint-cern_legacy_facts-check'
  spec.version     = '1.0.1'
  spec.homepage    = 'https://gitlab.cern.ch/ai-config-team/puppet-lint-cern_legacy_facts-check'
  spec.license     = 'Apache-2.0'
  spec.author      = 'Steve Traylen'
  spec.email       = 'steve.traylen@cern.ch'
  spec.files       = Dir[
    'README.md',
    'LICENSE',
    'lib/**/*',
    'spec/**/*',
  ]
  spec.test_files  = Dir['spec/**/*']
  spec.summary     = 'A puppet-lint plugin to check you are not using legacy facts like $::operatingsystemmajorrelease'
  spec.description = <<-EOF
  A pupet-lint to check you are not using legacy facts like `$::operatingsystemmajorrelease`
  or `$facts['operatingsystemmajorrelease']`. You should use the new structured facts like
  `$facts['os']['name']` instead
  EOF

  spec.add_dependency             'puppet-lint', '~> 2.4'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rspec-its', '~> 1.0'
  spec.add_development_dependency 'rspec-json_expectations'
  spec.add_development_dependency 'rspec-collection_matchers', '~> 1.0'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'simplecov'
end
