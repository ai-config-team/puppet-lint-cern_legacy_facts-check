require 'spec_helper'

describe 'cern_legacy_facts' do
  let(:msg) { 'cern legacy fact' }
  context 'with fix disabled' do
    context "fact variable using modern $facts['os']['family'] hash" do
      let(:code) { "$facts['os']['family']" }

      it 'should not detect any problems' do
        expect(problems).to have(0).problem
      end
    end

    context "fact variable using legacy $operatingsystemmajorrelease" do
      let(:code) { "$operatingsystemmajorrelease" }

      it 'should not detect any problems' do
        expect(problems).to have(0).problem
      end
    end

    context "fact variable using legacy $facts['operatingsystemmajorrelease']" do
      let(:code) { "$facts['operatingsystemmajorrelease']" }

      it 'should only detect a single problem' do
        expect(problems).to have(1).problem
      end
    end

    context "fact variable using legacy $::operatingsystemmajorrelease" do
      let(:code) { "$::operatingsystemmajorrelease" }

      it 'should only detect a single problem' do
        expect(problems).to have(1).problem
      end
    end



    context "fact variable in interpolated string \"${::operatingsystemmajorrelease}\"" do
      let(:code) { '"start ${::operatingsystemmajorrelease} end"' }

      it 'should only detect a single problem' do
        expect(problems).to have(1).problem
      end
    end

    context "fact variable using legacy variable in double quotes \"$::operatingsystemmajorrelease\"" do
      let(:code) { "\"$::operatingsystemmajorrelease\"" }

      it 'should only detect a single problem' do
        expect(problems).to have(1).problem
      end
    end

    context 'fact variable using legacy facts hash variable in interpolation' do
      let(:code) { %("${facts['operatingsystemmajorrelease']}") }

      it 'detects a single problem' do
        expect(problems).to have(1).problem
      end
    end
  end


  context 'with fix enabled' do
    before do
      PuppetLint.configuration.fix = true
    end

    after do
      PuppetLint.configuration.fix = false
    end

    context "fact variable using legacy $operatingsystemmajorrelease" do
      let(:code) { "$operatingsystemmajorrelease" }

      it 'should not detect any problems' do
        expect(problems).to have(0).problem
      end

    end

    context "fact variable using legacy $facts['operatingsystemmajorrelease']" do
      let(:code) { "$facts['operatingsystemmajorrelease']" }

      it 'should only detect a single problem' do
        expect(problems).to have(1).problem
      end

      it 'should fix the problem' do
        expect(problems).to contain_fixed(msg).on_line(1).in_column(1)
      end

      it 'should use the facts hash' do
        expect(manifest).to eq("$facts['os']['release']['major']")
      end
    end

    context "fact variable using legacy $::operatingsystemmajorrelease" do
      let(:code) { "$::operatingsystemmajorrelease" }

      it 'should only detect a single problem' do
        expect(problems).to have(1).problem
      end

      it 'should fix the problem' do
        expect(problems).to contain_fixed(msg).on_line(1).in_column(1)
      end

      it 'should use the facts hash' do
        expect(manifest).to eq("$facts['os']['release']['major']")
      end
    end

  end
end
